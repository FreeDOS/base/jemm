# JEMM386

Jemm386 is an Expanded Memory Manager, based on the source of FreeDOS EMM386. * needs only 192 bytes DOS memory * needs very little extended memory * is faster * works with MS-DOS, FreeDOS and EDR-DOS * will use features implemented on later CPUs to further increase speed * unlike FreeDOS Emm386 Jemm386 has full VDS support * can be loaded from the command line

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## JEMM.LSM

<table>
<tr><td>title</td><td>JEMM386</td></tr>
<tr><td>version</td><td>5.85</td></tr>
<tr><td>entered&nbsp;date</td><td>2025-01-10</td></tr>
<tr><td>description</td><td>Jemm386 is an Expanded Memory Manager</td></tr>
<tr><td>summary</td><td>Jemm386 is an Expanded Memory Manager, based on the source of FreeDOS EMM386. * needs only 192 bytes DOS memory * needs very little extended memory * is faster * works with MS-DOS, FreeDOS and EDR-DOS * will use features implemented on later CPUs to further increase speed * unlike FreeDOS Emm386 Jemm386 has full VDS support * can be loaded from the command line</td></tr>
<tr><td>keywords</td><td>HIMEM + EMM386, STABLE, COMPATIBILITY, memory manager, jemmex, himemx</td></tr>
<tr><td>author</td><td>Japheth, Tom Ehlert, Michael Devore (FreeDosStuff -at- devoresoftware.com)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/Baron-von-Riedesel/Jemm</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Artistic License</td></tr>
</table>
